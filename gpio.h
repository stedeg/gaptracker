#pragma once

#pragma region [ Pin out ]

#define GPIO_BEEP	    4				   // emette un beep sonoro
#define GPIO_LIGHTHOUSE A3				   // accende la segnalazione luminosa
#define GPIO_LED_RED    A1				   // accende un led rosso
#define GPIO_LED_GREEN  A2				   // accende un led verde

// SD
#define GPIO_DS1307_CS  53				   // collegato alla lettore sd

#pragma endregion

#pragma region [ Pin in ]

#define GPIO_START	    A0				   // avvia le letture
#define GPIO_MENU	    A4				   // scorre il menu

#pragma endregion
