﻿/*
Name:		GapTracker.ino
Created:	2/19/2020 10:43:10 AM
Author:	sdegan
*/

#include <Arduino.h>
#include "gt.h"

////////////////////////////////////////////////////////////
// I2C Address
////////////////////////////////////////////////////////////
#define ADO 1
#if ADO
const int8_t MPU_I2C_ADDR = 0x69;       // addres of the MPU-6050 device
#else
const int8_t MPU_I2C_ADDR = 0x68;
#endif
const int8_t RTC_I2C_ADDR = 0x68;       // address of the DS-1307 device
const int8_t OLED_I2C_ADDR = 0x3C;      // address of the SSD-1306 device
const int8_t VL6180X_I2C_ADDR = 0x29;   // address of the VL6180X device

// ISR Timer lettura sensori
const int OCR1_50HZ = 1249;			// 50Hz timer 1 con precaler 256 (campioni al secondo)
const int OCR1_1HZ = 62499;			// 1Hz timer 1 con precaler 256 (campioni al secondo)

GapTracker* _gt = NULL;

void InitializeGpio()
{
    _gt->InitializeGpio(Gpio::Beep, GPIO_BEEP, OUTPUT, LOW);
    _gt->InitializeGpio(Gpio::LedLighthouse, GPIO_LIGHTHOUSE, OUTPUT, LOW);
    _gt->InitializeGpio(Gpio::LedRed, GPIO_LED_RED, OUTPUT, LOW);
    _gt->InitializeGpio(Gpio::LedGreen, GPIO_LED_GREEN, OUTPUT, LOW);
    _gt->InitializeGpio(Gpio::ButtonStart, GPIO_START, INPUT_PULLUP);
    _gt->InitializeGpio(Gpio::ButtonMenu, GPIO_MENU, INPUT_PULLUP);
}


//
// Configurazione del timer
// interrupt frequency (Hz) = (Arduino clock speed 16,000,000Hz) / (prescaler * (compare match register + 1))
// https://www.arduinoslovakia.eu/application/timer-calculator
//
void InitializeISR()
{
    noInterrupts();								// disable all interrupts

                                                // Sensors
    TCCR1A = 0;									// set entire TCCR1A register to 0
    TCCR1B = 0;									// same for TCCR1B
    TCNT1 = 0;									// initialize counter value to 0

    OCR1A = OCR1_1HZ;
    //OCR1A = OCR1_50HZ;						// = 16000000 / (256 * 50) (must be <65536) 
                                                // 16MHz freq. arduino
                                                // 256 prescaler
                                                // 50 Hz

    TCCR1B |= (1 << WGM12);						// turn on CTC mode
    TCCR1B |= (1 << CS12);;						// Set prescaler 256
    TIMSK1 |= (1 << OCIE1A);					// enable timer compare interrupt

    interrupts();								// enable all interrupts
}

// Eseguta ogni secondo dall'interrupt interno
// Si occupa di fare il countdown del timer
// Il timer deve essere veloce per non avere problemi
// pertanto imposto dei flag per abilitare l'esecuzione
// di procedure complesse all'interno del loop
//
ISR(TIMER1_COMPA_vect)
{
    if (_gt->Start())
    {
        _gt->Sample(ParamDirection::SET, true);
    }

    // Visualizzo stato batteria ogni secondo
    _gt->RefreshBattery(ParamDirection::SET, true);

    Serial.print("*");
}

//
// The setup function runs once when you press reset or power the board
//
void setup()
{
    Serial.begin(9600);

    while (!Serial) {
        ; // wait for serial port to connect. Needed for Leonardo only
    }

    _gt = new GapTracker();

    InitializeGpio();

    _gt->SwitchLed(Led::Red, true);
    _gt->SwitchLed(Led::Green, false);
    _gt->SwitchLed(Led::Lighthouse, false);

    // Setup dei device... se qualcosa va storno il led rosso lampeggia
    _gt->SetupRTC(RTC_I2C_ADDR);		        // configura RTC (va configurato con l'orario)
    _gt->SetupMPU(MPU_I2C_ADDR);		        // configura giroscopio
    _gt->SetupOLED(OLED_I2C_ADDR);              // configura il display
    _gt->SetupVL6180X(VL6180X_I2C_ADDR);
    _gt->SetupSD(GPIO_DS1307_CS);			    // configura lettore sd

    _gt->ReadConfig();

    InitializeISR();

    Serial.println(_gt->ErrorFlags());

    if (_gt->ErrorFlags() == 0)
    {
        _gt->SwitchLed(Led::Red, true);
        _gt->DisplaySplash();
        delay(2000);
        _gt->DisplayInfo();
    }
    else
    {
        _gt->BlinkLed(Led::Red, true, true);	 // in caso di errori accende il led rosso ad intermittenza
        _gt->DisplayError();
        _gt->Melody((int*)ERROR_MELODY);
    }

    Serial.println("------------ SETUP FINISHED ------------");
}

//
// The loop function runs over and over again until power down or reset
//
void loop()
{
    // Gestisce la pressione dei pulsanti
    _gt->PressButtons();

    // Legge i sensori quando richiesto il campionamento
    if (_gt->Sample())
    {
        Serial.println("Sample");

        _gt->Sample(ParamDirection::SET, false);

        // Leggo dai sensori e salvo riga nella sd
        if (_gt->WriteLog() == false)
        {
            Serial.println("Errore WriteLog()");

            // Disabilità il campionamento e visualizza l'errore
            _gt->Start(ParamDirection::SET, false);
            _gt->BlinkLed(Led::Red, true, true);
            _gt->DisplayError();
        }
    }

    if (_gt->RefreshBattery())
    {
        _gt->RefreshBattery(ParamDirection::SET, false);

        _gt->DisplayBattery();
    }

    _gt->GoBlink();
}

