#include "gt.h"

#pragma region [ CTor ]

GapTracker::GapTracker()
{
    _startEnabled = false;			        // avvia campionamento
    _readSample = false;				    // registra un nuovo campione
    _refreshBattery = false;

    _blinkLighthouseEnabled = false;	    // accende luce di avviso
    _blinkLighthouseOn = false;		        // luce accesa
    _blinkLighthouseMillims = millis();
    _blinkLighthouseSpeedMillims = TIMEMS_LIGHTHOUSE;

    _blinkRedEnabled = false;			    // intermittenza led rosso
    _blinkRedOn = false;				    // led rosso acceso
    _blinkRedMillims = millis();

    _blinkGreenEnabled = false;		        // intermittenza led verde
    _blinkGreenOn = false;			        // led verde acceso
    _blinkGreenMillims = millis();

    strcpy(_lastErrorMessage, "");
    _errorFlags = 0;				        // flag degli errori

    _display = new Adafruit_SSD1306(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);
    _vlx = new Adafruit_VL6180X();
}

#pragma endregion

#pragma region [ Properties ]

// Restituisce true se il sistema � avviato
bool GapTracker::Start(ParamDirection direction = ParamDirection::GET, bool enable = true)
{
    if (direction == ParamDirection::SET) {
        _startEnabled = enable;
    }

    return _startEnabled;
}

// Abilita il campionamento one shot
bool GapTracker::Sample(ParamDirection direction = ParamDirection::GET, bool enable = true)
{
    if (direction == ParamDirection::SET) {
        _readSample = enable;
    }

    return _readSample;
}

// Aggiorna lo stato della batteria
bool GapTracker::RefreshBattery(ParamDirection direction = ParamDirection::GET, bool enable = true)
{
    if (direction == ParamDirection::SET) {
        _refreshBattery = enable;
    }

    return _refreshBattery;
}


// Imposta il flag degli errori
int GapTracker::ErrorFlags(ParamDirection direction = ParamDirection::GET, int errors = 0)
{
    if (direction == ParamDirection::SET) {
        _errorFlags = errors;
    }

    return _errorFlags;
}

// Imposta il messaggio dell'ultimo errore
char* GapTracker::LastErrorMessage(ParamDirection direction = ParamDirection::GET, char* message = NULL)
{
    if (direction == ParamDirection::SET) {
        strncpy(_lastErrorMessage, message, 100);
    }

    return _lastErrorMessage;
}

#pragma endregion

#pragma region [ GPIO ]

// Configura tutti i gpio
void GapTracker::InitializeGpio(Gpio gpio, int pinout, int direction = OUTPUT, int level = LOW)
{
    if (direction == OUTPUT) {
        pinMode(pinout, OUTPUT);
        digitalWrite(pinout, level);
    }
    else if (direction == INPUT || direction == INPUT_PULLUP) {
        pinMode(pinout, direction);
    }

    if (gpio == Gpio::Beep)
        _gpioBeep = pinout;
    else if (gpio == Gpio::LedLighthouse)
        _gpioLighthouse = pinout;
    else if (gpio == Gpio::LedRed)
        _gpioRedLed = pinout;
    else if (gpio == Gpio::LedGreen)
        _gpioGreenLed = pinout;
    else if (gpio == Gpio::ButtonStart)
        _gpioStartButton = pinout;
    else if (gpio == Gpio::ButtonMenu)
        _gpioMenuButton = pinout;
}

#pragma endregion

#pragma region  [ Setup ]

//
// Configurazione del modulo RTC (SD1307)
//
void GapTracker::SetupRTC(int address)
{
    _rtcAddress = address;

    Wire.begin();				 	// Inizializza la porta I2C
    _RTC.begin();				 	// Inizializza la comunicazione con il modulo RTC
    if (!_RTC.isrunning()) {
        Serial.println("RTC is not running - you may need to replace the battery or check connections");
        strcpy(_lastErrorMessage, "RTC is not running");
        _errorFlags |= ERROR_RTC;
    }

    //PrintRTC();
}

//
// Configurazione del modulo SD (YL-30)
//
void GapTracker::SetupSD(int pinCS)
{
    Serial.print("Initializing SD card...");

    // make sure that the default chip select pin is set to
    // output, even if you don't use it:
    // pinMode(10, OUTPUT);
    pinMode(pinCS, OUTPUT);

    // see if the card is present and can be initialized:
    if (!SD.begin(pinCS)) {
        Serial.println("Card failed, or not present");
        strcpy(_lastErrorMessage, "SD init failed");
        _errorFlags |= ERROR_SD;
        return;
    }
    Serial.println("card initialized.");
}

//
// Configurazione del giroscopio (GY 521)
//
void GapTracker::SetupMPU(int address)
{
    _mpuAddress = address;

    Serial.begin(9600);
    Wire.begin();
    Wire.beginTransmission(address); // Begins a transmission to the I2C slave (GY-521 board)
    Wire.write(0x6B); // PWR_MGMT_1 register
    Wire.write(0); // set to zero (wakes up the MPU-6050)
    Wire.endTransmission(true);
}

//
// Inizializza il display
//
void GapTracker::SetupOLED(int address)
{
    _oledAddress = address;

    if (!_display->begin(SSD1306_SWITCHCAPVCC, address)) { // Address 0x3D for 128x64
        Serial.println(F("SSD1306 allocation failed"));
        strcpy(_lastErrorMessage, "Display init failed");
        _errorFlags |= ERROR_OLED;
        return;
    }

    char log[256];
    sprintf(log, "Display size: W=%d, H=%d", _display->width(), _display->height());
    Serial.println(log);

    _display->clearDisplay();
    _display->display();
}

//
// Configurazione sensore di prossimit� (VL6180X)
//
void GapTracker::SetupVL6180X(int address)
{
    _vlxAddress = address;

    Serial.println("Sensor VL6180x test!");
    if (!_vlx->begin()) {
        Serial.println("Failed to find sensor VL6180X");
        while (1);
    }
    Serial.println("Sensor VL6180X found!");

    //strcpy(_lastErrorMessage, "Distance sensor init failed");
    //_errorFlags |= ERROR_PROXIMITY;
}

bool GapTracker::ReadConfig()
{
    bool ok = false;
    
    _measurementsSize = 0;

    char* data = NULL;

    // Apre il file di configurazione in lettura
    File configFile = SD.open(CONFIGURATION_FILE, FILE_READ);
    if (configFile) 
    {
        Serial.println("success opened configuration file in sd.");

        // Legge il contenuto del file
        char* offset = NULL;
        int size = 0;
        while (configFile.available()) {
            size += 100;
            data = (char*)realloc(data, size);
            if (!offset)
                offset = data;
         
            memset(offset, 0, 100);
            
            offset += configFile.readBytes(offset, 100);
        }

        configFile.close();

        Serial.println("configuration file contents:");

        Serial.println(data);

        ok = true;
    }

    // Se non esiste o � vuoto lo crea con il contenuto di default
    if (data == NULL || (data != NULL && strlen(data) == 0))
    {
        ok = false;

        data = (char*)DEFAULT_CONFIG;

        Serial.println("missing configuration file or empty...");

        // Scrive il json di default nel file di configurazione
        configFile = SD.open(CONFIGURATION_FILE, FILE_WRITE);
        if (configFile) 
        {
            Serial.println("json wrote:");
            Serial.println(data);

            configFile.print(data);

            configFile.flush();
            configFile.close();

            Serial.println("configuration file created in sd");

            ok = true;
        }
        else
        {
            Serial.println("error create config file in sd");
            strcpy(_lastErrorMessage, "Error creation config file");
            _errorFlags |= ERROR_SD;
        }
    }

    if (ok)
    {
        Serial.println("Configuration contents parsing...");
        Serial.println("Configuration file contents:");
        Serial.println(data);

        // Parser del json
        DynamicJsonBuffer jsonBuffer;
        JsonObject& root = jsonBuffer.parseObject(data);
        if (root.success()) 
        {
            strcpy(_machineName, root["name"]);

            JsonArray& jsonMeasurements = root["measurements"];

            for (int i = 0; i < MEASUREMENTS_SIZE; i++)
            {
                _measurementsConfiguration[i].enabled = false;

                if (i < jsonMeasurements.size())
                {
                    JsonObject& jsonMeasurement = jsonMeasurements[i];
                    JsonObject& jsonRange = jsonMeasurement["range"];
                    JsonObject& jsonLight = jsonMeasurement["light"];
                    JsonObject& jsonSound = jsonMeasurement["sound"];

                    _measurementsConfiguration[i].enabled = true;
                    _measurementsConfiguration[i].rangeMin = jsonRange["min"].as<int>();
                    _measurementsConfiguration[i].rangeMax = jsonRange["max"].as<int>();
                    _measurementsConfiguration[i].lightSwitched = jsonLight["switched"] == "on";
                    _measurementsConfiguration[i].lightSpeed = jsonLight["speed"].as<int>();
                    _measurementsConfiguration[i].soundSwitched = jsonSound["switched"] == "on";
                    _measurementsConfiguration[i].soundSpeed = jsonSound["speed"].as<int>();

                    _measurementsSize++;

                    Serial.println("added measurement");
                }
            }

            Serial.println(_machineName);

            Serial.println("Elenco measurements:");
            for (int i = 0; i < MEASUREMENTS_SIZE; i++) {
                sprintf(_tmp, "%d, %d, %d, %d, %d, %d, %d", 
                    _measurementsConfiguration[i].enabled,
                    _measurementsConfiguration[i].rangeMin,
                    _measurementsConfiguration[i].rangeMax,
                    _measurementsConfiguration[i].lightSwitched,
                    _measurementsConfiguration[i].lightSpeed,
                    _measurementsConfiguration[i].soundSwitched,
                    _measurementsConfiguration[i].soundSpeed
                );

                Serial.println(_tmp);
            }

            ok = true;
        }
        else
        {
            // file di configurazione non valido
            Serial.println("Configuration file not valid");
            strcpy(_lastErrorMessage, "Configuration file not valid");
            _errorFlags |= ERROR_CONFIG_JSON;
        }
    }
    else
    {
        // file di configurazione non valido
        Serial.println("Configuration file inacessible");
        strcpy(_lastErrorMessage, "Configuration file inacessible");
        _errorFlags |= ERROR_CONFIG_FILE;
    }

    if (data != NULL)
        free(data);

    return ok;
}

#pragma endregion

#pragma region  [ Read ]

//
// Legge misure dal giroscopio
//
void GapTracker::ReadOrientation(GyroMeasure& measure)
{
    Wire.beginTransmission(_mpuAddress);
    Wire.write(0x3B); // starting with register 0x3B (ACCEL_XOUT_H) [MPU-6000 and MPU-6050 Register Map and Descriptions Revision 4.2, p.40]
    Wire.endTransmission(false); // the parameter indicates that the Arduino will send a restart. As a result, the connection is kept active.
    Wire.requestFrom(_mpuAddress, 7 * 2, true); // request a total of 7*2=14 registers

    int16_t accelerometerX = Wire.read() << 8 | Wire.read(); // reading registers: 0x3B (ACCEL_XOUT_H) and 0x3C (ACCEL_XOUT_L)
    int16_t accelerometerY = Wire.read() << 8 | Wire.read(); // reading registers: 0x3D (ACCEL_YOUT_H) and 0x3E (ACCEL_YOUT_L)
    int16_t accelerometerZ = Wire.read() << 8 | Wire.read(); // reading registers: 0x3F (ACCEL_ZOUT_H) and 0x40 (ACCEL_ZOUT_L)
    int16_t temperature = Wire.read() << 8 | Wire.read(); // reading registers: 0x41 (TEMP_OUT_H) and 0x42 (TEMP_OUT_L)
    int16_t gyroX = Wire.read() << 8 | Wire.read(); // reading registers: 0x43 (GYRO_XOUT_H) and 0x44 (GYRO_XOUT_L)
    int16_t gyroY = Wire.read() << 8 | Wire.read(); // reading registers: 0x45 (GYRO_YOUT_H) and 0x46 (GYRO_YOUT_L)
    int16_t gyroZ = Wire.read() << 8 | Wire.read(); // reading registers: 0x47 (GYRO_ZOUT_H) and 0x48 (GYRO_ZOUT_L)

    measure.angleRoll = AngleRoll(accelerometerX, accelerometerY, accelerometerZ);
    measure.anglePitch = AnglePitch(accelerometerX, accelerometerY, accelerometerZ);
    measure.temperature = temperature / 340.00 + 36.53;
}

//
// Restitusice la distanza misurata dal sensore in millimetri e la luminosit�
//
void GapTracker::ReadDistance(uint8_t& range, float& lux)
{
    lux = _vlx->readLux(VL6180X_ALS_GAIN_5);

    Serial.print("Lux: "); Serial.println(lux);

    range = _vlx->readRange();
    
    uint8_t status = _vlx->readRangeStatus();

    if (status == VL6180X_ERROR_NONE) {
        Serial.print("Range: "); Serial.println(range);
    }

    if ((status >= VL6180X_ERROR_SYSERR_1) && (status <= VL6180X_ERROR_SYSERR_5)) {
        Serial.println("System error");
    }
    else if (status == VL6180X_ERROR_ECEFAIL) {
        Serial.println("ECE failure");
    }
    else if (status == VL6180X_ERROR_NOCONVERGE) {
        Serial.println("No convergence");
    }
    else if (status == VL6180X_ERROR_RANGEIGNORE) {
        Serial.println("Ignoring range");
    }
    else if (status == VL6180X_ERROR_SNR) {
        Serial.println("Signal/Noise error");
    }
    else if (status == VL6180X_ERROR_RAWUFLOW) {
        Serial.println("Raw reading underflow");
    }
    else if (status == VL6180X_ERROR_RAWOFLOW) {
        Serial.println("Raw reading overflow");
    }
    else if (status == VL6180X_ERROR_RANGEUFLOW) {
        Serial.println("Range reading underflow");
    }
    else if (status == VL6180X_ERROR_RANGEOFLOW) {
        Serial.println("Range reading overflow");
    }
}

#pragma endregion

#pragma region [ Led ]

//
// Blink della luce di avviso e dei led
//
void GapTracker::GoBlink()
{
    // Blink luce bianca
    if (_blinkLighthouseEnabled)
    {
        if (millis() - _blinkLighthouseMillims > _blinkLighthouseSpeedMillims)
        {
            Serial.println("Lighthouse blink");
            _blinkLighthouseOn = !_blinkLighthouseOn;
            _blinkLighthouseMillims = millis();
            digitalWrite(_gpioLighthouse, _blinkLighthouseOn ? HIGH : LOW);
        }
    }

    // Blink led rosso
    if (_blinkRedEnabled)
    {
        if (millis() - _blinkRedMillims > TIMEMS_LED_RED)
        {
            Serial.println("Red blink");
            _blinkRedOn = !_blinkRedOn;
            _blinkRedMillims = millis();
            digitalWrite(_gpioRedLed, _blinkRedOn ? HIGH : LOW);
        }
    }

    // Blink led verde
    if (_blinkGreenEnabled)
    {
        if (millis() - _blinkGreenMillims > TIMEMS_LED_GREEN)
        {
            Serial.println("Green blink");
            _blinkGreenOn = !_blinkGreenOn;
            _blinkGreenMillims = millis();
            digitalWrite(_gpioGreenLed, _blinkGreenOn ? HIGH : LOW);
        }
    }
}

//
// Accensione e spegnimento di un led
//
void GapTracker::SwitchLed(Led led, bool on)
{
    Serial.println("SwitchLed()");

    if (led == Led::Lighthouse) {
        _blinkLighthouseEnabled = false;
        digitalWrite(_gpioLighthouse, on ? HIGH : LOW);
    }
    else if (led == Led::Red) {
        _blinkRedEnabled = false;
        digitalWrite(_gpioRedLed, on ? HIGH : LOW);
    }
    else if (led == Led::Green) {
        _blinkGreenEnabled = false;
        digitalWrite(_gpioGreenLed, on ? HIGH : LOW);
    }
}

//
// Lampeggio di un led
//
void GapTracker::BlinkLed(Led led, bool blink, bool reset = false)
{
    if (reset) {
        _blinkLighthouseEnabled = false;
        _blinkRedEnabled = false;
        _blinkGreenEnabled = false;
        digitalWrite(_gpioLighthouse, LOW);
        digitalWrite(_gpioRedLed, LOW);
        digitalWrite(_gpioGreenLed, LOW);
    }

    if (led == Led::Lighthouse) {
        _blinkLighthouseEnabled = blink;
        digitalWrite(_gpioLighthouse, LOW);
    }
    else if (led == Led::Red) {
        _blinkRedEnabled = blink;
        digitalWrite(_gpioRedLed, LOW);
    }
    else if (led == Led::Green) {
        _blinkGreenEnabled = blink;
        digitalWrite(_gpioGreenLed, LOW);
    }
}

#pragma endregion

#pragma region [ Display ]

//
// Scrive nel display
//
void GapTracker::PrintDisplay(char rows[4][80], bool clear = true)
{
    Serial.println("PrintDisplay()");

    if (_display != NULL)
    {
        if (clear) {
            CleanDisplay();
        }

        _display->setTextSize(1.1);
        _display->setTextColor(WHITE);

        for (int i = 0; i < 4; i++)
        {
            _display->setCursor(0, TOPBAR_HEIGHT + i * LINE_HEIGHT);
            _display->println(rows[i]);
            Serial.println(rows[i]);
        }

        _display->display();
    }
}

#pragma endregion

#pragma region [ Events ]

void GapTracker::PressButtons()
{
    // avvio o spengo la lettura dai sensori
    if (digitalRead(_gpioStartButton) == LOW)
    {
        PressedStart();

        delay(200);
    }
    else if (digitalRead(_gpioMenuButton) == LOW)
    {
        PressedMenu();

        delay(200);
    }
}

//
// Gestisce la pressione del pulsante START
//
void GapTracker::PressedStart()
{
    Serial.println("PressedStart()");

    // Solo se non ci sono stati degli errori durante il setup
    if (_errorFlags == 0)
    {
        _startEnabled = !_startEnabled;

        Serial.print("started="); Serial.println(_startEnabled);

        if (_startEnabled)
        {
            // Letture avviate
            DateTime now = _RTC.now();

            char logFile[80];
            //sprintf(logFile, "gt01_%04u%02u%02u_%02u%02u%02u.dat", now.year(), now.month(), now.day(), now.hour(), now.minute(), now.second());
            sprintf(logFile, "gt01_123.txt");

            Serial.println(logFile);

            SwitchLed(Led::Red, false);
            SwitchLed(Led::Green, false);
            BlinkLed(Led::Lighthouse, false);

            if (OpenLog(logFile))
            {
                Melody((int*)START_MELODY);

                SwitchLed(Led::Green, true);
                BlinkLed(Led::Lighthouse, true);

                _currentMeasurementsIndex = -1;

                // Abilita il timer 1
                _startEnabled = true;
            }
            else
            {
                Serial.println("PressedStart(): Error");

                BlinkLed(Led::Red, true);	 // in caso di errori accende il led rosso ad intermittenza

                strncpy(_lastErrorMessage, "Open SD Error!", 100);

                DisplayError();

                Melody((int*)ERROR_MELODY);
            }
        }
        else
        {
            Melody((int*)STOP_MELODY);

            // Disabilita il timer 1
            _startEnabled = false;

            Serial.println("Chiudo il log");

            // Chiude il file di log
            CloseLog();

            Serial.println("Log chiuso");

            SwitchLed(Led::Red, true);
            SwitchLed(Led::Green, false);
            BlinkLed(Led::Lighthouse, false);
        }
    }
}

//
// Gestisce la pressione del pulsante START
//
void GapTracker::PressedMenu()
{
    if (_startEnabled)
    {
        DisplaySensors();
    }
    else
    {
        if (_currentMenu == Menu::Splash) {
            DisplaySplash();
            _currentMenu = Menu::Informations;
        }
        else if (_currentMenu == Menu::Informations) {
            DisplayInfo();
            _currentMenu = Menu::Splash;
        }
    }
}

#pragma endregion

#pragma region [ Log ]

//
// Apre file di log
//
bool GapTracker::OpenLog(char* file)
{
    bool ok = false;

    _logFile = SD.open(file, FILE_WRITE);
    if (_logFile) {
        Serial.println("success opened file in sd");
        //_logFile.seek(0);
        ok = true;
    }
    else
    {
        Serial.println("error opening file in sd");
        strcpy(_lastErrorMessage, "Error opening file");
        _errorFlags |= ERROR_SD;
    }

    return ok;
}

//
// Chiude il file di log
//
void GapTracker::CloseLog()
{
    if (_logFile) {
        _logFile.flush();
        _logFile.close();
    }
}

//
// Scrive le misure nel datalog:
// YYYYMMSS|HHMMSS|AX,AY,AZ|GX,GY,GZ|GT|D
//
bool GapTracker::WriteLog()
{
    bool ok = false;

    if (_logFile)
    {
        ReadOrientation(_currentGyro);
        ReadDistance(_currentRange, _currentLux);
        _currentTimestamp = _RTC.now();

        DisplaySensors();

        ManageMeasurement();

        //
        // Aggiunge una misura nel log
        //
        char log[500];
        sprintf(log, "GT01|%04u-%02u-%02u|%02u:%02u:%02u|%u|%u|%u|%u|%u",
            _currentTimestamp.year(), _currentTimestamp.month(), _currentTimestamp.day(),
            _currentTimestamp.hour(), _currentTimestamp.minute(), _currentTimestamp.second(),
            _currentGyro.angleRoll, _currentGyro.anglePitch,
            _currentRange, _currentLux, _currentGyro.temperature
        );
        
        ok = (_logFile.println(log) > 0);
        if (!ok) {
            Serial.println("error writing file in sd");
            strcpy(_lastErrorMessage, "Error writing file");
            _errorFlags |= ERROR_SD;
        }

        Serial.println(log);
    }

    return ok;
}

#pragma endregion

#pragma region [ Display ]


void GapTracker::DisplaySensors()
{
    //
    // Visualizza i parametri dei sensori sul display
    //
    char rows[4][80] = { 0, 0, 0, 0 };
    //sprintf(rows[1], "%04u/%02u/%02u %02u:%02u:%02u",
    //    _currentTimestamp.year(), _currentTimestamp.month(), _currentTimestamp.day(),
    //    _currentTimestamp.hour(), _currentTimestamp.minute(), _currentTimestamp.second()
    //);
    char lux[20];
    dtostrf(_currentLux, 6, 2, lux);

    sprintf(rows[0], "Roll: %d Pitch: %d", _currentGyro.angleRoll, _currentGyro.anglePitch);
    sprintf(rows[1], "Range: %d mm", _currentRange);
    sprintf(rows[2], "Lux: %s", lux);
    sprintf(rows[3], "Temp: %d C", _currentGyro.temperature);
    PrintDisplay(rows);
}

//
// Visualizza l'error sul display
//
void GapTracker::DisplayError(char *msg)
{
    Serial.println("DisplayError()");

    DateTime now = _RTC.now();
    char rows[4][80] = { 0, 0, 0, 0 };
    sprintf(rows[0], "%04u/%02u/%02u %02u:%02u:%02u",
        now.year(), now.month(), now.day(),
        now.hour(), now.minute(), now.second()
    );

    if (msg != NULL)
        sprintf(rows[1], "%s", msg);
    else
        sprintf(rows[1], "%s", LastErrorMessage());

    PrintDisplay(rows);
}


void GapTracker::DisplaySplash()
{
    CleanDisplay();

    _display->drawBitmap(0, TOPBAR_HEIGHT, &LOGO[0], LOGO_WIDTH, LOGO_HEIGHT, 1);
    _display->setTextSize(1.8);
    _display->setTextColor(WHITE);
    _display->setCursor(50, TOPBAR_HEIGHT);
    _display->println("GapTracker");
    _display->setTextSize(1.1);
    _display->setCursor(60, 25);
    _display->println("(c) 2020");

    char rows[4][80] = { 0, 0, 0, 0 };
    sprintf(rows[2], "Model: %s", MACHINE_MODEL);
    sprintf(rows[3], "Id: %s", _machineName);

    PrintDisplay(rows, false);
}


//
// Visualizza il device sul display
//
void GapTracker::DisplayInfo()
{
    Serial.println("DisplayInfo()");

    char rows[4][80] = { 0, 0, 0, 0 };
    sprintf(rows[0], "Model: %s", MACHINE_MODEL);
    sprintf(rows[1], "Id: %s", _machineName);
    PrintDisplay(rows);
}

void GapTracker::DisplayBattery()
{
    //Serial.println("DisplayBattery()");

    int percBattery = 30;

    _display->fillRect(0, 0, SCREEN_WIDTH, TOPBAR_HEIGHT, SSD1306_BLACK);

    // Visualizzo l'ora in alto a sinistra
    DateTime now = _RTC.now();
    char row[80] = { 0 };
    sprintf(row, "%02u:%02u:%02u", now.hour(), now.minute(), now.second());
    _display->setTextSize(1.1);
    _display->setTextColor(WHITE);
    _display->setCursor(0, 0);
    _display->println(row);

    // Visualizzo lo stato della batteriea in alto a destra
    if (percBattery > 80)
        _display->drawBitmap(SCREEN_WIDTH - BATTERY_WIDTH - 1, 0, &BATTERY4[0], BATTERY_WIDTH, BATTERY_HEIGHT, 1);
    else if (percBattery > 60)
        _display->drawBitmap(SCREEN_WIDTH - BATTERY_WIDTH - 1, 0, &BATTERY3[0], BATTERY_WIDTH, BATTERY_HEIGHT, 1);
    else if (percBattery > 40)
        _display->drawBitmap(SCREEN_WIDTH - BATTERY_WIDTH - 1, 0, &BATTERY2[0], BATTERY_WIDTH, BATTERY_HEIGHT, 1);
    else if (percBattery > 20)
        _display->drawBitmap(SCREEN_WIDTH - BATTERY_WIDTH - 1, 0, &BATTERY1[0], BATTERY_WIDTH, BATTERY_HEIGHT, 1);
    else
        _display->drawBitmap(SCREEN_WIDTH - BATTERY_WIDTH - 1, 0, &BATTERY0[0], BATTERY_WIDTH, BATTERY_HEIGHT, 1);

    _display->display();
}

void GapTracker::CleanDisplay()
{
    _display->fillRect(0, TOPBAR_HEIGHT - 1, SCREEN_WIDTH, SCREEN_HEIGHT - TOPBAR_HEIGHT, SSD1306_BLACK);
}

#pragma endregion

#pragma region []

void GapTracker::ManageMeasurement()
{
    for (int i = 0; i < MEASUREMENTS_SIZE; i++)
    {
        measurement* m = &_measurementsConfiguration[i];
        if (m->enabled)
        {
            if (_currentRange >= m->rangeMin && _currentRange <= m->rangeMax)
            {
                if (_currentMeasurementsIndex != i)
                {
                    if (_measurementsConfiguration[i].lightSwitched)
                    {
                        _blinkLighthouseSpeedMillims = m->lightSpeed;

                        sprintf(_tmp, "_blinkLighthouseSpeedMillims: %d", _blinkLighthouseSpeedMillims);
                        Serial.println(_tmp);

                    }
                    if (m->soundSwitched)
                    {
                        // _measurementsConfiguration[i].soundSpeed
                    }

                    _currentMeasurementsIndex = i;

                    sprintf(_tmp, "Range: %d-%d; Light: %d,%d; Sound: %d,%d;", \
                        m->rangeMin, m->rangeMax, m->lightSwitched, m->lightSpeed, m->soundSwitched, m->soundSpeed);
                    Serial.println(_tmp);
                }

                break;
            }
        }
    }
}

#pragma endregion

#pragma region [ Debug ]
//
// Stampa l'orario del RTC
//
//
void GapTracker::PrintRTC()
{
    DateTime now = _RTC.now();
    Serial.print(now.year(), DEC);
    Serial.print('/');
    Serial.print(now.month(), DEC);
    Serial.print('/');
    Serial.print(now.day(), DEC);
    Serial.print(' ');
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    Serial.print(now.second(), DEC);
    Serial.println();
}

#pragma endregion

#pragma region [ Audio ]

//
// Suona una melodia
//
void GapTracker::Melody(int* song)
{

    while (true)
    {
        int note = *song++;
        int duration = *song++;
        if (note == 0 && duration == 0)
            break;

        Serial.println(printf("%d, %d, ", note, duration));

        duration = 1000 / duration;
        tone(_gpioBeep, note, duration);
        delay(duration * 1.30);
        noTone(_gpioBeep);

    }
}


#pragma endregion

#pragma region [ Functions ]

int GapTracker::AngleRoll(double AcX, double AcY, double AcZ) {
    double A = AcX;
    double B = sqrt((AcY * AcY) + (AcZ * AcZ));

    return (int)(atan2(A, B) * 180 / 3.14);
}

int GapTracker::AnglePitch(double AcX, double AcY, double AcZ) {
    double A = AcY;
    double B = sqrt((AcX * AcX) + (AcZ * AcZ));

    return (int)(atan2(A, B) * 180 / 3.14);
}

#pragma endregion

// converts int16 to string. Moreover, resulting strings will have the same length in the debug monitor.
//char* convert_int16_to_str(int16_t i) {
//    sprintf(tmp_str, "%6d", i);
//    return tmp_str;
//}



