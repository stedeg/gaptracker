#pragma once

#pragma region [ Includes ]

#include <ArduinoJson.h>
#include <SPI.h>
#include <Wire.h>
#include <RTClib.h>
#include <SD.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_VL6180X.h>
#include "gpio.h"
#include "songs.h"
#include "resource.h"
#include "measurement.h"

#pragma endregion

static const char MACHINE_MODEL[] = "GT01";

static const char CONFIGURATION_FILE[] = "gtconf2.txt";

#pragma region [ Errors Mask ]

#define ERROR_RTC		    0x00000001
#define ERROR_SD		    0x00000010
#define ERROR_MPU		    0x00000100
#define ERROR_PROXIMITY	    0x00001000
#define ERROR_OLED			0x00010000
#define ERROR_CONFIG_FILE   0x00100000
#define ERROR_CONFIG_JSON   0x00200000

#pragma endregion

#pragma region [ Screen ]

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define TOPBAR_HEIGHT  16
#define LINE_HEIGHT     13

#pragma endregion

#pragma region [ Enums ]

// Direzione parametri
enum ParamDirection {
    GET,
    SET
};

// Identifica i led
enum Led {
    Red,
    Green,
    Lighthouse,
};

// Identifica i gpio 
enum Gpio {
    Beep,
    LedLighthouse,
    LedRed,
    LedGreen,
    ButtonStart,
    ButtonMenu,
};

// Pagina del menu
enum Menu {
    Splash,             // logo
    Informations,       // id e modello dispositivo
    Sensors,            // Valore dei sensori
    About,              // Autore (si attiva dopo aver premuto a lungo > 5 secondi)
};


#pragma endregion

#pragma region [ Structures ] 

// Informazioni lettura giroscopio
struct GyroMeasure {
    int angleRoll;
    int anglePitch;
    int temperature;
};

#pragma endregion

#pragma region [ Measurements ]

#define MEASUREMENTS_SIZE     10

#pragma endregion

#pragma region [ Class ]

class GapTracker
{
private:
    // Intermittenza luce di avviso e led
    const int TIMEMS_LIGHTHOUSE = 1000;	    // intermittenza luce avviso in ms
    const int TIMEMS_LED_RED = 500;		    // intermittenza led rosso in ms
    const int TIMEMS_LED_GREEN = 500;		// intermittenza led verde in ms

    // Gpio
    int _gpioBeep = -1;
    int _gpioLighthouse = -1;
    int _gpioRedLed = -1;
    int _gpioGreenLed = -1;
    int _gpioStartButton = -1;
    int _gpioMenuButton = -1;

    // Device
    Adafruit_SSD1306* _display = NULL;      // OLED
    RTC_DS1307 _RTC;                        // RTC
    File _logFile;                          // File di log (misurazioni)
    Adafruit_VL6180X* _vlx = NULL;          // VL6180X

    // Blink led
    bool _blinkLighthouseEnabled = false;	// accende il faro
    bool _blinkLighthouseOn = false;		// faro acceso
    unsigned long _blinkLighthouseMillims;
    unsigned long _blinkLighthouseSpeedMillims;

    bool _blinkRedEnabled = false;		    // intermittenza led rosso
    bool _blinkRedOn = false;			    // led rosso acceso
    unsigned long _blinkRedMillims;

    bool _blinkGreenEnabled = false;		// intermittenza led verde
    bool _blinkGreenOn = false;			    // led verde acceso
    unsigned long _blinkGreenMillims;

    // Variabili interne
    bool _startEnabled = false;
    bool _readSample = false;
    bool _refreshBattery = false;

    // Errori
    int _errorFlags = 0;
    char _lastErrorMessage[100]; // = { 0 };

    // I2C address
    int _mpuAddress = 0x69;
    int _rtcAddress = 0x68;
    int _oledAddress = 0x3C;
    int _vlxAddress = 0x29;

    int AngleRoll(double AcX, double AcY, double AcZ);
    int AnglePitch(double AcX, double AcY, double AcZ);

    Menu _currentMenu = Menu::Splash;

    GyroMeasure _currentGyro;
    uint8_t _currentRange;
    float _currentLux;
    DateTime _currentTimestamp;

    char _tmp[100];

    char _machineName[50];
    int _measurementsSize = 0;
    measurement _measurementsConfiguration[MEASUREMENTS_SIZE];      // max 5 range in array configurations

    int _currentMeasurementsIndex = -1;

public:
    GapTracker();

    void SetupRTC(int address);
    void SetupSD(int pinCS);
    void SetupMPU(int address);
    void SetupOLED(int address);
    void SetupVL6180X(int address);

    bool Start(ParamDirection direction = ParamDirection::GET, bool enable = true);
    bool Sample(ParamDirection direction = ParamDirection::GET, bool enable = true);
    bool RefreshBattery(ParamDirection direction = ParamDirection::GET, bool enable = true);

    void ReadOrientation(GyroMeasure& measure);
    void ReadDistance(uint8_t& range, float& lux);

    void GoBlink();

    void SwitchLed(Led led, bool on);
    void BlinkLed(Led led, bool blink, bool reset = false);

    void PrintDisplay(char rows[4][80], bool clear);

    void PressButtons();
    void PressedStart();
    void PressedMenu();

    bool OpenLog(char* file);
    void CloseLog();
    bool WriteLog();

    void CleanDisplay();
    void DisplaySensors();
    void DisplayError(char *msg = NULL);
    void DisplayInfo();
    void DisplaySplash();
    void DisplayBattery();

    void ManageMeasurement();

    void PrintRTC();

    void Melody(int* song);

    bool ReadConfig();

    int ErrorFlags(ParamDirection direction = ParamDirection::GET, int errors = 0);
    char* LastErrorMessage(ParamDirection direction = ParamDirection::GET, char* message = NULL);

    void InitializeGpio(Gpio gpio, int pinout, int direction = OUTPUT, int level = LOW);

};

#pragma endregion
